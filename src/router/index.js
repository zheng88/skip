import Vue from "vue";
import VueRouter from "vue-router";
import TodoShow from "../views/TodoShow.vue";
import Todo from "../views/Todo.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Todo",
    component: Todo
  },
  {
    path: "/todo/:id",
    name: "TodoShow",
    component: TodoShow
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
