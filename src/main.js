import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// usage bootstrap
import jQuery from "jquery";
import "popper.js";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
window.jQuery = window.$ = jQuery;

Vue.config.productionTip = false;

import NewCom from "@/components/NewCom";
Vue.component("new-com", NewCom);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
